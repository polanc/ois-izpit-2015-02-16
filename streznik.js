var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});

app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});

app.get('/api/dodaj', function(req, res) {
	var DST = req.query.davcnaStevilka;
	var IME = req.query.ime;
	var PRI = req.query.priimek;
	var ADR = req.query.naslov;
	var HST = req.query.hisnaStevilka;
	var PST = req.query.postnaStevilka;
	var LOC = req.query.kraj;
	var CON = req.query.drzava;
	var POK = req.query.poklic;
	var TST = req.query.telefonskaStevilka;
	
	var RESPONSE = "";
	var CHECK = false;
	if (DST == null || DST.length == 0 || IME.length == 0 || IME == null || PRI == null || PRI.length == 0|| ADR == null || ADR.length == 0 || HST == null || HST.length == 0 || PST == null || PST.length == 0|| LOC == null || LOC.length == 0 || CON == null || CON.length == 0 || POK == null || POK.length == 0 || TST == null || TST.length == 0) {
		RESPONSE = "Napaka pri dodajanju osebe!";
	}
	for (var i in uporabnikiSpomin) {
		if (DST == uporabnikiSpomin[i]["davcnaStevilka"]) {
			CHECK = true;
		}
	}
	if (CHECK == true) {
		RESPONSE = "Oseba z davčno številko " + DST + " že obstaja!<br/><a href='javascript:window.history.back()'>Nazaj</a>";
		res.redirect('/api/seznam');
	}
	else {
		var L = uporabnikiSpomin.length;
		uporabnikiSpomin[L+1]["davcnaStevilka"] = DST;
		uporabnikiSpomin[L+1]["ime"] = IME;
		uporabnikiSpomin[L+1]["priimek"] = PRI;
		uporabnikiSpomin[L+1]["naslov"] = ADR;
		uporabnikiSpomin[L+1]["hisnaStevilka"] = HST;
		uporabnikiSpomin[L+1]["postnaStevilka"] = PST;
		uporabnikiSpomin[L+1]["kraj"] = LOC;
		uporabnikiSpomin[L+1]["drzava"] = CON;
		uporabnikiSpomin[L+1]["poklic"] = POK;
		uporabnikiSpomin[L+1]["telefonskaStevilka"] = TST;
	}
	res.send(RESPONSE);
});

app.get('/api/brisi', function(req, res) {
	var DST = req.query.davcnaStevilka;
	var SEND = "";
	var X = -1;
	var CHECK = false;
	if (DST == null){
		SEND =  "Oseba z davčno številko <Davcna_stevilka> ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>";
	}
	else {
		for (var i in uporabnikiSpomin) {
			if (DST == uporabnikiSpomin[i]["davcnaStevilka"]){
				CHECK = true;
				 X = i;
			}
		}
		if (CHECK == false){
			SEND = "Napačna zahteva!";
		}
		else if (CHECK == true){
			uporabnikiSpomin.splice(X, 1);
		res.redirect('/api/seznam');
		}
	}
	res.send(SEND);
});

var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');

var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];